﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VenosheBot.Areas.v1.Models
{
    public class UserInfo
    {
        public int Id { get; set; }
        public long TelegramId { get; set; }
        public String TelegramUser { get; set; }
        public String Name { get; set; }
        public String Family { get; set; }
        public String Phone { get; set; }
        public String ProfileUri { get; set; }
        public String RegisterDate { get; set; }
        public String TodayePurchase { get; set; }
        public String Validation { get; set; }
    }
}