﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VenosheBot.Areas.v1.Models
{
    public class DBHelper
    {
        public static String getWord(long user_id)
        {
            return "Glaring";
        }

        public static List<String> getEnDefenition(long user_id, String word)
        {
            List<String> retValue = new List<string>();
            retValue.Add("Glaring");
            retValue.Add("adjective: giving out or reflecting a strong or dazzling light.");
            retValue.Add("verb: stare in an angry or fierce way.");
            return retValue;
        }

        public static List<String> getFaDefenition(long user_id, String word)
        {
            List<String> retValue = new List<string>();
            retValue.Add("Glaring");
            retValue.Add("adjective: درخشش");
            retValue.Add("verb: خیره نگاه کردن");
            return retValue;
        }

        public static List<String> getEnExample(long user_id, String word)
        {
            List<String> retValue = new List<string>();
            retValue.Add("adjective: the glaring sun");
            retValue.Add("verb: she glared at him, her cheeks flushing");
            return retValue;
        }

        public static List<String> getGif(long user_id, String word)
        {
            List<String> retValue = new List<string>();
            retValue.Add("https://38.media.tumblr.com/9f6c25cc350f12aa74a7dc386a5c4985/tumblr_mevmyaKtDf1rgvhr8o1_500.gif");
            retValue.Add("A man is glaring, and someone with sunglasses appears.");
            return retValue;
        }
    }
}