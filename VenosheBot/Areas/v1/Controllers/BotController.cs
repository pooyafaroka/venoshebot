﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using VenosheBot.Areas.v1.Models;
using VenosheBot.TelegramHelper;

namespace VenosheBot.Areas.v1.Controllers
{
    /// <summary>
    /// Webhook: https://api.telegram.org/bot465563893:AAF1CngxeOhgBhC5WExf6xfj1adRT4aWw6w/SetWebHook?url=https://pooyafaroka.localtunnel.me/v1/Bot/
    /// Port: 1316
    /// ngrok: ngrok http 1316 -host-header="localhost:1316"
    /// NPM: iis-lt --port 1316 --subdomain pooyafaroka
    /// </summary>
    public class BotController : Controller, IMessageProcessor
    {
        public TelegramInterface _telegram_interface;

        public ActionResult Index()
        {
            _telegram_interface = new TelegramInterface(WebConfigurationManager.AppSettings["API"]);
            _telegram_interface.ParseUpdate(Request.InputStream, this);

            return Content("{\"ok\":\"false\", \"result\":\"No data\"}", "application/json");
        }

        public String OnRecievedInlineQuery(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedCallbackQuery(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVoice(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedAudio(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVideo(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVideoNote(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedDocument(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedPhoto(TelegramTypes.X_Update xCallback)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedContact(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedLocation(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedVenue(TelegramTypes.X_Update xMessage)
        {
            throw new NotImplementedException();
        }

        public String OnRecievedMessage(TelegramTypes.X_Update xMessage)
        {
            long user_id = xMessage.message.from.id;
            String word = DBHelper.getWord(user_id);
            return null;
        }

    }
}